<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        
        $this->call(ClientTableSeeder::class); //usando o arquivo de Seed da Tabela Client (fica mais organizado desta forma)
 
        Model::reguard();
    }
}
