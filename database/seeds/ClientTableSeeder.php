<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//Video 2.4 - time: 3:40
    	//movidos do arquivo DatabaseSeeder.php para cá....

       //apagar dados antigos
        \CodeProject\Client::truncate(); 

        //inserir 10 registros fake
        factory(\CodeProject\Client::class, 10)->create();

    }
}
